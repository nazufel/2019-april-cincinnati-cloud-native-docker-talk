// 01-docker/00-singleStageBuild/main.go

// define the package
package main

// import other packages
import (
	"fmt"
	"net/http"
)

// route hander that prints a string back in the http response
func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hello Cincinnati Cloud Native!!!</h1>")
}

// main function
func main() {
	// http route that calls the index handler on the index route
	http.HandleFunc("/", index)

	// start the webserver and listen on port 3000, use the default router.
	http.ListenAndServe(":8000", nil)
}
