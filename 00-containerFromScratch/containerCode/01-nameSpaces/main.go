// 00-noIsolation.go

// MAINTAINER: Ryan Ross ryanthebossross@gmail.com

// PURPOSE: Build a container and take command line arguements. Looking to demonstate giving linux commands their own Process ID (PID).
// However, this container has no isolation!!! The MAINTAINER reccomends running in a VM. See Vagrant file and provisioner to set that up.

/*
Example commands:

* echo cli args from the container *
[vagrant@localhost 00-noNameSpaces]$ go run main.go run echo hello cloud native!
running[echo hello cloud native!]
hello cloud native!

* change the hostname of the container *
[root@localhost 00-noNameSpaces]# hostname cloudnative
[root@localhost 00-noNameSpaces]# hostname
cloudnative
[root@localhost 00-noNameSpaces]# exit
exit
[root@localhost 00-noNameSpaces]# hostname
cloudnative

*/

//--------------------------------------------------//
package main

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"
)

// main fucntion and container run commands
func main() {
	// if the first cli command is "run", call run
	switch os.Args[1] {
	case "run":
		run()
	// panic
	default:
		panic("what?")
	}
}

// run command takes cli args, builds container, and executes the cli args
func run() {
	// debug message with all cli flags
	fmt.Printf("running%v\n", os.Args[2:])

	// create the container
	// "/proc/self/exe" basically a fork-exec that passes in the child() with os.Args
	cmd := exec.Command(os.Args[2], os.Args[3:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	// namespace the container
	cmd.SysProcAttr = &syscall.SysProcAttr{
		// If using VS Code on a Mac (like I am), Cloneflags may show to fail, but this works fine in Linux. Since this is being built and run on linux, no big deal.
		// syscall.CLONE_NEWUTS = clones the Unix Timesharing System into the container and protects the machine's hostname outside the container.
		Cloneflags: syscall.CLONE_NEWUTS,
	}

	// run the container with commands and check for errors
	must(cmd.Run())
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}

/*
That was cool, but it's not ready for production. We need namespaces. Move onto directory ../01-nameSpaces.
*/
