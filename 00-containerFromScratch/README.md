# Containers from Scratch
This directory contains the code required to stand up a CEntOS 7 VM in Vagrant, have a shared directory between the host and guest vm, provision what the guest VM needs, and the container from scratch code. Building a container from scratch demonstrates the not so black magic of process isolation through namespaces.

## Directory Layout
This directory contains several folders and files.

### Container Code
The directory contains the actual code for the containers. It should be self explanitory and the code is well commented. Essentially, the first directory is the code for an unisolated process. The second holds code that protects the hostname using the [Unix Timesharing System](https://www.bell-labs.com/usr/dmr/www/cacm.pdf).

#### 00-noIsolation
Vagrant creates a shared directory at `../containersFromScratch` on the host and `/vagrant_data` on the guest. Use `vagrant ssh` to connect to the guest and navigate to this directory. 

There is only one file, `main.go`. One way to run a Golang file is is the `go run main.go` syntax. The code anticiates two further arguments:

* `run`
* \<linux command\>

A complete way to run the code will be `go run main.go run /bin/bash`. This will execute the file and open up a `/bin/bash` terminal within the container, but it isn't self evident. There is not protection of the hostname as we will see later. Change the hostname by running `hostname <hostname>` and both the container hostname and the VM's hostname will change. Exit out of the session and type `hostname`. the hostname of the VM is now the same as the container since we aren't protecting it. Now let's see this process with the protection of [syscall](https://golang.org/pkg/syscall/).

#### 01-nameSpaces
Protecting the host machine's hostname requires syscall. Change directory into this directory and run `go run main.go run /bin/bash`. A new bash session opens. Now type `hostname <hostname>`. Exit out of the session again and check the hostname. The VM's hostname should be the same since syscall is protecting it.   

#### Provisioner
Using Ansible and Ansible Galaxy as the provisioning step for the VM. You need to have Ansible installed on your machine. Most software repos have it. 

Next, install the Ansible Galaxy roles by `ansible-galaxy install -r <path/to/requirements.yml>`. This will install all roles defined in the file, like `requirements.txt` for `pip`.

Ansible can be called from the `Vagrantfile`.

#### Vagrantfile
[Vagrant](www.vagrantup.com) is a codified way to interact with popular VM management tools such as [Virtualbox](https://www.virtualbox.org/). The `Vagrantfile` defines VM specs.

It is envoked by `vagrant up`, but you can also use the `--provision` flag to use Ansible to provision the VM. Vagrant must be called from the directory the Vagrantfile is in.

## Citation

All code and segment inspiration came from Liz Rice's talk at [Container Camp](https://www.youtube.com/watch?v=Utf-A4rODH8)